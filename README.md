# Application Notes: Simple Driver for the LSM9DS1 

## Assumptions

- Using a nRF52840 SOC
- Using the nordic SDK v15.02

## Environment Setup

- Download and install GNU arm-gcc
- Add arm-none-eabi-gcc to your path
- Download and install nrfjprog bin tools
  - Required for programming the device
- Install SEGGER JLINK bin tools
  - NOTE: This is required to to use SEGGER RTT, giving the SOC printf capabilities

## Example Hardware Setup

To setup hardware for this example you simply have to connect:

| LSM9DS1 | nRF52840 |
|---------|----------|
| Vin | Vdd |
| Gnd | Gnd |
| Scl | p0 26 |
| Sda | p0 27 |
| Sdo_ag | p0 28 |
| Csag | P1 01 |

## Building and Loading Image

Connect the SOC via the programming port then checkout this examples root directory in a terminal, and run the following commands

```bash
make build
```

This command builds compiles the image, mostly used as a check to see if the code compiles successfully.

```bash
make flash
```

this command builds and flashes the image onto the SOC.

## Viewing Output/Connecting the SEGGER RTT

In a seperate terminal you need to connect to the JLink device, to do so use the following command.

```bash
JLinkExe -device NRF52 -speed 16000 -if SWD -autoconnect 1
```

Once connected to the device in a separate terminal we need to start an RTT client

```bash
JLinkRTTClient
```

You should now be receiving output from the SOC

## Using Driver

Using the driver is simple. After setting up the SPI peripheral all that needs to be done is call the init function and then calling the sample function will provide you with a sample

### Configuring and Initilizing SPI Peripheral

Our driver uses nordics spi driver in order to communicate with the lsm9ds1. Our driver does not automatically configure the spi peripheral due to the large number of hardware configurations. Also it is very easy to configure the SPI peripheral.

The first thing that needs to be done is creating the spi peripheral instance. This done as follows. It is generally a good idea to make your instance a global variable so it dosen't fall out of scope.

```C
static nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(0);
```

Once you have an instance you need to configure the spi parameters like which pins it uses on the SOC. It is important to note that our driver handles the CS pin so it's paramount that the .ss_pin parameter is set to `NRF_DRV_SPI_PIN_NOT_USED` to ensure that the peripheral doesn't take control of the CS pin.

```C
nrf_drv_spi_config_t spi_config = {                 
        .sck_pin      = NRF_GPIO_PIN_MAP(0, 26),                
        .mosi_pin     = NRF_GPIO_PIN_MAP(0, 27),                
        .miso_pin     = NRF_GPIO_PIN_MAP(0, 28),                
        .ss_pin       = NRF_DRV_SPI_PIN_NOT_USED,                
        .irq_priority = SPI_DEFAULT_CONFIG_IRQ_PRIORITY,         
        .orc          = 0xFF,                                    
        .frequency    = NRF_DRV_SPI_FREQ_1M,                     
        .mode         = NRF_DRV_SPI_MODE_3,                      
        .bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,         
    };
```

Once you have a config and an instance you simply need to call the init function as follows.

```C
nrf_drv_spi_init(&spi, &spi_config, NULL, NULL);
```

### Configuring and Initilizing the lsm9ds1

To use our driver you need to create an instance of a device structure. The device needs a refernece to the spi instance its connected to, the port of the CS pin, the pin number of the CS pin, an ID, and be enabled by setting the enabled parameter to true.

```C
lsm9ds1_device_t device = {
	.spi = &spi,
	.cs_port = NRF_P1,
	.cs_pin = 1,
	.device_id = 0,
	.enabled = true
};
```

Once you have the device you need to call the init function. This function writes all the control register and sets up the device for polling samples.

**NOTE: Samples cannot be polled faster than 109Hz**

After its been initilized you merely call the sample function and it will grab all the data.

```C
lsm9ds1_output_data_t data;

// Data is written to the lsm9ds1_output_data_t structure 'data'
lsm9ds1_sample(&device, &data);
```

### Using the data

Once you have a sample using the data is easy. The `lsm9ds1_output_data_t` structure format is as follows. It has all three axises of the accel and gyro and an ID field for identifying which device the data was sampled from.

```C
typedef struct LSM9DS1_OUTPUT_DATA lsm9ds1_output_data_t;
struct LSM9DS1_OUTPUT_DATA
{
    uint8_t device_id;
    union LSM9DS1_OUTPUT accel_x;
    union LSM9DS1_OUTPUT accel_y;
    union LSM9DS1_OUTPUT accel_z;
    union LSM9DS1_OUTPUT gyro_x;
    union LSM9DS1_OUTPUT gyro_y;
    union LSM9DS1_OUTPUT gyro_z;
};
```

Each axises is stored is stored in a union `LSM9DS1_OUTPUT` which is defined as follows

```C
union LSM9DS1_OUTPUT
{
    struct
    {
        uint8_t low;
        uint8_t high;
    } bytes;

    int16_t value;
};
```

The struct bytes is used by the driver to correctly place the incoming data from the lsm9ds1 in memory as it uses 8-bit registers the high and low bytes need to be combined to properly format the data, this structure inside the union accomplishes this automatically.

So to access the sample data you simply need to...

```C
int16_t x_axis_accel = data.accel_x.value;
```
