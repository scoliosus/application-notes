#ifndef LSM9DS1_DRIVER_H
#define LSM9DS1_DRIVER_H

#include "lsm9ds1_portmap.h"

/**
 * @brief Function for configuring the lsm9ds1 to sample both the accel and gyro
 * 			at 100Hz in Block Data Update mode. BDU mode means data is not written
 * 			to the output registers until previous data has been read.
 *
 * @param[in] device  Pointer to lsm9ds1 device description.
 */
void lsm9ds1_init(lsm9ds1_device_t *device);

/**
 * @brief Function for reading a sample from the lsm9ds1 device. It reads data from
 * 			all axises of the accel and gyro and places it into the provided sample
 * 			data structure.
 *
 * @param[in] device  Pointer to lsm9ds1 device description.
 * @param[out] data	  Pointer to the output data stucture that will store the sample
 */
bool lsm9ds1_sample(lsm9ds1_device_t *device, lsm9ds1_output_data_t *data);

/**
 * @brief Function for averaging two samples
 *
 * @param[in/out] 	sample1  Pointer to a output data stucture. The average is stored here
 * @param[out] 		sample2  Pointer to a output data stucture
 */
void lsm9ds1_avg_samples(lsm9ds1_output_data_t *sample1, lsm9ds1_output_data_t *sample2);

#endif // !LSM9DS1_DRIVER_H`