#include <stdbool.h>
#include <stdint.h>

#include "nrf_drv_clock.h"
#include "nrf_gpio.h"
#include "nrf_drv_spi.h"
#include "nrf_delay.h"
#include "app_error.h"
#include "SEGGER_RTT.h"

#include "lsm9ds1_driver.h"

static nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(0);

int main(void)
{
    ret_code_t err_code;
    err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_spi_config_t spi_config = {                 
        .sck_pin      = NRF_GPIO_PIN_MAP(0, 26),                
        .mosi_pin     = NRF_GPIO_PIN_MAP(0, 27),                
        .miso_pin     = NRF_GPIO_PIN_MAP(0, 28),                
        .ss_pin       = NRF_DRV_SPI_PIN_NOT_USED,                
        .irq_priority = SPI_DEFAULT_CONFIG_IRQ_PRIORITY,         
        .orc          = 0xFF,                                    
        .frequency    = NRF_DRV_SPI_FREQ_1M,                     
        .mode         = NRF_DRV_SPI_MODE_3,                      
        .bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,         
    };

    lsm9ds1_device_t device = {
        .spi = &spi,
        .cs_port = NRF_P1,
        .cs_pin = 1,
        .device_id = 0,
        .enabled = true
    };

    lsm9ds1_output_data_t data;

    NRF_P0->OUT = NRF_P0->OUT | GPIO_OUT_PIN28_Msk
                              | GPIO_OUT_PIN27_Msk;

    NRF_P0->DIR = NRF_P0->DIR | GPIO_DIR_PIN28_Msk
                              | GPIO_DIR_PIN26_Msk;

    NRF_P1->DIR = NRF_P1->DIR | GPIO_DIR_PIN1_Msk;

    NRF_P1->OUT = NRF_P1->OUT | GPIO_OUT_PIN1_Msk;

    nrf_drv_spi_init(&spi, &spi_config, NULL, NULL);
    lsm9ds1_init(&device);

    while (true)
    {
        lsm9ds1_sample(&device, &data);

        SEGGER_RTT_printf(0, "DEVICE: %d\n", data.device_id);
        SEGGER_RTT_printf(0, "Accel x: %d y: %d z: %d\n", data.accel_x.value, data.accel_y.value, data.accel_z.value);
        SEGGER_RTT_printf(0, "Gyro  x: %d y: %d z: %d\n", data.gyro_x.value, data.gyro_y.value, data.gyro_z.value);

        nrf_delay_ms(1000);
    }
}
